// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdlib.h>

long long main(int argc, char ** argv){
    long long a = atol(argv[1]);
    long long b = atol(argv[2]);
    long long c = atol(argv[3]);
    int ai = (int)a;
    a /= b;
    a %= c;
    ai /= (int)b;
    ai %= (int)c;
    a += ai;
    return a;
}
