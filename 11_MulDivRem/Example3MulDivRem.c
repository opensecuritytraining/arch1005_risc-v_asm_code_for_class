// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

int main(){
    int a = 0x11111111;
    a *= 0x22222222;
    a /= 0x33333333;
    a %= 101;
    return a;
}
