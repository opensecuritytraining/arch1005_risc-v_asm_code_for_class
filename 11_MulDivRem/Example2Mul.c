// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#define uint64 unsigned long long
__int128 main(){
    __int128 a = 0xd155ec7ed;
    a *= 0xde1ec7ab1e;
    return a;
}
