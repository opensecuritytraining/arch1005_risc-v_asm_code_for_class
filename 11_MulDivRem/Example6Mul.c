// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdlib.h>

__int128 main(int argc, char ** argv){
    __int128 a128 = atol(argv[1]);
    unsigned long long b64 = atol(argv[2]);
    a128 *= b64;
    a128 *= a128;
    return a128;
}
