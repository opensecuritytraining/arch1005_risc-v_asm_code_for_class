// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

int main(int argc, char ** argv){
    int a = 0x5a1adbad;
    signed char * b = (signed char *)&a;
    return (b[3] - b[0]);
}
