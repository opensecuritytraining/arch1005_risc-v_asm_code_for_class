// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#define uint64 unsigned long long

uint64 func( uint64 a, uint64 b, uint64 c, uint64 d,
          uint64 e, uint64 f, uint64 g, uint64 h,
          uint64 i, uint64 j)
{
    uint64 z = a+b-c+d-e+f-g+h-i+j;
    return z;
}

int main(){
    return func(00,11,22,33,44,55,66,77,88,99);
}
