// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

int func3(){
    return 0xb0ba;
}
int func2(){
    func3();
    return 0xee15;
}
int func(){
    func2();
    return 0xbeef;
}
int main(){
    func();
    return 0xf00d;
}
