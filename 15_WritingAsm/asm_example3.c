// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdio.h>
#include <stdlib.h>

void main(int argc, char ** argv){
    long long myVar = strtoll(argv[1], NULL, 16);
    asm("nop");
    // alt way to get value into register from C variable
    asm("ld t0, %0" : : "m" (myVar));
    asm("bgtz t0, target1");
    printf("myVar = %llx is less than or equal zero\n", myVar);
    asm("j target2");
    asm("target1:");
    printf("myVar = %llx is greater than zero\n", myVar);
    asm("target2:");
    asm("nop");
}
