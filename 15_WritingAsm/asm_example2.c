// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdio.h>
void main(){
    long long myVar = 0x00112233aabbccdd;
    asm("nop");
    // alt value into register from C variable
    asm("ld t0, %0" : : "m" (myVar));
    // change value
    asm("addi t0, t0, 0x123");
    // alt value into C variable from register
    asm("sd t0, %0" : "=m" (myVar));
    asm("nop");
    printf("myVar = %llx\n", myVar);
}
