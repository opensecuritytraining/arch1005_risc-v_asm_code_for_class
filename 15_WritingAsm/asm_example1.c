// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdio.h>
void main(){
    long long myVar = 0x00112233aabbccdd;
    asm("nop");
    // value into register from C variable
    asm("mv t0, %0" : : "r" (myVar)); // That's not valid RISC-V asm!!!
    // change value
    asm("addi t0, t0, 0x123");
    // value into C variable from register
    asm("mv %0, t0" : "=r" (myVar)); // That's not valid RISC-V asm!!!
    asm("nop");
    printf("myVar = %llx\n", myVar);
}
