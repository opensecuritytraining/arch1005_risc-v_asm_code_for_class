// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdlib.h>

int main(int argc, char ** argv){
  unsigned int a = 0x11;
  unsigned int b = 0x22;
  int c = atoi(argv[1]);
  a -= b << c;
  return a;
}
