// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdlib.h>

int main(int argc, char ** argv){
  unsigned int a = 1;
  int b = 1, c = 1;
  a = b << atoi(argv[1]);
  b = a >> atoi(argv[2]);
  c = b >> atoi(argv[3]);
  return c;
}
