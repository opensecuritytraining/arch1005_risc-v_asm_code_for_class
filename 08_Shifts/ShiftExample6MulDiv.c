// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdlib.h>

long main(int argc, char **argv){

    long a, b;
    unsigned long c;
    a = atoi(argv[1]);
    c = strtoul(argv[1], NULL, 16);
    a = a * 8;
    b = a / 16;
    c = c / 4 + b;
    return c;
}
