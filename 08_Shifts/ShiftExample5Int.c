// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

int main(int argc, char ** argv){
  unsigned int a = 1;
  int b = 2, c = 3;
  a >>= 1;
  b >>= 2;
  c <<= 3;
  return c;
}
