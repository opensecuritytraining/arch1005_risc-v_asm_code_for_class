// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

unsigned long main(){
  unsigned long a, b, c;
  a = 0x5;
  b = a << 4;
  c = b >> 3;
  return c;
}
