// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdio.h>
#define uint64 unsigned long long

unsigned long long main() {
    uint64 i = 0x50da;
    uint64 j = 0xc0ffee;
    uint64 k = 0x7ee707a11ed;
    k ^= ~(i & j) | 0x7ab00;
    return k;
}
