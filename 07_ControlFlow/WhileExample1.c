// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdio.h>
#include <stdlib.h>

long main(int argc, char ** argv){
    long a = atol(argv[1]);
    while(a > 0){
        a--;
        printf("Hi!\n");
    }
    return 0x501dface;
}
