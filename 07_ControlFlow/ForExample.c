// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv){
    long i = atol(argv[1]);
    for(i; i > 0; i--){
       printf("Hi!\n");
    }
    return 0x501dface;
}
