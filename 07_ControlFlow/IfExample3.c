// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdlib.h>

unsigned long main(int argc, char ** argv){
    long input = atol(argv[1]);
    long a = 2;
    unsigned long b = 2;
    if(a < input){
        return 2;
    }
    if(b < input){
        return 3;
    }
    return 0xb3375bea75bee75;
}
