// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

long main(){
    long a = -1, b = 2;
    if(a == b){
        return 1;
    }
    if(a > b){
        return 2;
    }
    if(a < b){
        return 3;
    }
    return 0xdefea7;
}
