// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdio.h>

int main(){
    goto mylabel;
    printf("skipped\n");
mylabel:
    printf("goto ftw!\n");
    return 0xb01dface;
}
