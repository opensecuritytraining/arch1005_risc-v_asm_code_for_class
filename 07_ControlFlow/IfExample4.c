// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

#include <stdlib.h>

long main(int argc, char ** argv){
    long a = atol(argv[1]);
    if(a != 0){
        return 1;
    }
    return 0xdece17;
}
