// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

long long main(){
    long long a = 0xaaaaaaaaaaaaaaaa;
    int b = 0xbbbbbbbb;
    long long * a_ptr = &a;
    int * b_ptr = &b;
    return *a_ptr + *b_ptr;
}
