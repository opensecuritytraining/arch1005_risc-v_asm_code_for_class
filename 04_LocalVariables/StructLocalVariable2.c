// This Source Code Form is subject to the terms of
// the Mozilla Public License, v. 2.0.

typedef struct mystruct{
    short a;
    long long b[3];
    long long c;
    long long d[3];
    unsigned short a2;
    char e;
    unsigned char f;
} mystruct_t;

long long main(){
    mystruct_t foo;
    foo.a = 0xaaaa;
    foo.b[0] = 0;
    foo.b[1] = 0;
    foo.b[2] = 0;
    foo.c = 0xcccccccccccccccc;
    foo.a2 = 0xa2a2;
    foo.e = 0xee;
    foo.f = 0xff;
    foo.d[0] = foo.a + foo.a2;
    foo.d[2] = foo.e + foo.f;
    return foo.d[2];
}
