// This Source Code Form is subject to the terms of 
// the Mozilla Public License, v. 2.0.

long long main(){
    long long a[6];
    long long b = 0x60f7060074306563;
    a[1] = 0xaaaaaaaaaaaaaaaa;
    a[4] = a[1] + b;
    return a[4];
}
